#include <stdio.h>
#include <mpi.h>

#include <random>

const int MPI_array = 0;
const int MPI_sum = 1;
const int MPI_len = 2;


double *generate_arr(int len)
{
    std::random_device rand_device;
    std::mt19937 gen(rand_device());
    std::uniform_real_distribution<> distr(0,15);

    double *arr = new double [len];
    for(int i=0; i<len; i++){
        arr[i] = distr(gen);
    }

    return arr;
}


int main (int argc, char* argv[])
{
    int errCode;

    if ((errCode = MPI_Init(&argc, &argv)) != 0)
    {
        printf("errCode: %d\n", errCode);
        return errCode;
    }

    int myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);


    int mpi_size;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    printf("MPI_mpi_size == %d\n", mpi_size);

    if(myRank == 0){
        int count = 800000;
        double *v = generate_arr(count);
        double sum  =0;
        for (int i=0;i<count;i++){
            sum+= v[i]*v[i];
        }
        printf("У один потік: %f\n",sqrt(sum));

        for(int i=1; i<mpi_size; i++) {
            int start = count * i / mpi_size;
            int finish = count * (i + 1) / mpi_size;
            int problem_len = finish - start;

            MPI_Send(&problem_len, 1, MPI_INT, i, MPI_len, MPI_COMM_WORLD);
            MPI_Send(v + start, finish-start, MPI_DOUBLE, i,
                    MPI_array, MPI_COMM_WORLD);
        }

        sum = 0;
        int len = count/mpi_size;
        for (int i=0;i<len;i++){
            sum += v[i]*v[i];
        }

        for(int i=1; i<mpi_size; i++){
            double local_sum;
            MPI_Status sum_status;
            MPI_Recv(&local_sum, 1, MPI_DOUBLE, i, MPI_sum, MPI_COMM_WORLD, &sum_status);
            sum += local_sum;
        }
        sum = sqrt(sum);
        printf("В багато потоків:%f\n",sum);
    }


    if(myRank != 0){//запуск тільки на процесі процесу комутатора
        MPI_Status len_status, arr_status;

        int len;
        MPI_Recv(&len, 1, MPI_INT, 0, MPI_len, MPI_COMM_WORLD, &len_status);

        double *arr = new double[len];
        MPI_Recv(arr, len, MPI_DOUBLE, 0, MPI_array, MPI_COMM_WORLD, &arr_status);

        double sum = 0;
        for (int i=0;i< len;i++){
            sum += arr[i]* arr[i];
        }

        MPI_Send(&sum, 1, MPI_DOUBLE, 0, MPI_sum, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}
